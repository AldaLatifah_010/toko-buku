@extends('layouts.layout')

@section('title')
    Home
@endsection

@section('index')
    active
@endsection

@section('main')
<main class="site-main">
    <section class="hero-banner">
      <div class="container">
        <div class="row no-gutters align-items-center pt-60px">
          <div class="col-5 d-none d-sm-block">
            <div class="hero-banner__img">
              <img class="img-fluid" src="{{asset('assets/img/home/buku.jpeg')}}" alt="">
            </div>
          </div>
          <div class="col-sm-7 col-lg-6 offset-lg-1 pl-4 pl-md-5 pl-lg-0">
            <div class="hero-banner__content">
              <h4>Ayo Membaca Buku </h4>
              <h1>Cari Buku Kesukaan Kamu ! </h1>
              <p><i>Semua orang adalah pembaca buku, sebagian orang mungkin belum mendapatkan buku kesukaanya. </i></p>
              <p>Toko Buku Online Candy menyediakan aneka buku berkualitas dan terlengkap. Nikmati promo buku murah dengan pengiriman beragam.</p>
              <a class="button button-hero" href=" {{route('home.product')}} ">Browse Now</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section-margin calc-20px">
      <div class="container">
        <div class="section-intro pb-60px">
          <p> Membaca dengan pengalaman seru dengan buku terbaru kami.</p>
          <h2>Buku  <span class="section-intro__style">Terbaru</span></h2>
        </div>
        <div class="row">
          @foreach ($products as $row)
            <div class="col-md-6 col-lg-4 col-xl-3">
              <div class="card text-center card-product">
                <div class="card-product__img">
                  <img class="card-img" src="{{ asset('storage/products/' . $row->image) }}" alt="{{ $row->name }}">
                  <ul class="card-product__imgOverlay">
                    <li><button><i class="ti-search"></i></button></li>
                    <li><a href="{{ url('/product/' . $row->slug) }}"><button><i class="ti-shopping-cart"></i></button></a></li>
                    <li><button><i class="ti-heart"></i></button></li>
                  </ul>
                </div>
                <div class="card-body">
                  <p>{{ $row->category->name}}</p>
                  <h4 class="card-product__title"><a href="single-product.html">{{$row->name}}</a></h4>
                  <p class="card-product__price">Rp. {{ number_format($row->price) }}</p>
                </div>
              </div>
            </div>
          @endforeach

        </div>
      </div>
    </section>

  </main>
@endsection
